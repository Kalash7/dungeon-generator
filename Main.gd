extends Node2D

var tile_map : TileMap

const MAP_SIZE : Vector2 = Vector2(800,600)
const MAX_ROOM_SIZE : Vector2 = Vector2(6,6)
const MIN_ROOM_SIZE : Vector2 = Vector2(6,6)
const MAX_NUMBER_ROOMS : int = 50

var rooms : Array
var rng : RandomNumberGenerator

func _ready():
	tile_map = $TileMap
	rng = RandomNumberGenerator.new()
	tile_map.clear()
	create_map()
	draw_map()
	# DEBUG
	print('Number of rooms generated -> ',rooms.size())
	#for room in rooms:
	#	print("Room has start at: ",room.room_start, " and end at ",room.room_end, "\n")
	
	#print("map to local ---> ",tile_map.map_to_local(Vector2i(40,40)))
	#print("local to map ---> ",tile_map.local_to_map(Vector2i(142,175)))
	#tile_map.set_cell(0,tile_map.map_to_local(Vector2i(40,40)),0,Vector2i(0,2),0)

	
	
func create_map() -> void:
	for rooms in MAX_NUMBER_ROOMS:
		create_room()
	
	
func draw_map() -> void:
	for room in rooms:
		var start : int = room.position.x
		var start_y : int = room.position.y
		var end : int = room.end.x
	
		while start != end:
			tile_map.set_cell(0,tile_map.local_to_map(Vector2i(start,room.position.y)),tile_map.tile_set.get_source_id(0),Vector2i(0,2),0)
			tile_map.set_cell(0,tile_map.local_to_map(Vector2i(start,room.end.y)),tile_map.tile_set.get_source_id(0),Vector2i(0,2),0)
			if start_y < room.end.y:
				tile_map.set_cell(0,tile_map.local_to_map(Vector2i(room.position.x,start_y)),tile_map.tile_set.get_source_id(0),Vector2i(0,2),0)
				tile_map.set_cell(0,tile_map.local_to_map(Vector2i(room.end.x,start_y)),tile_map.tile_set.get_source_id(0),Vector2i(0,2),0)
			start += 1
			start_y += 1
		
	
		


func create_room() -> void:
	# Create room
	var room_position_x : int = rng.randi_range(0,MAP_SIZE.x)
	var room_position_y : int = rng.randi_range(0,MAP_SIZE.y)
	var room_end_x : int = room_position_x + rng.randi_range(4,8)
	var room_end_y : int = room_position_y + rng.randi_range(4,8)
	var room : Rect2i
	
	room.position = Vector2i(room_position_x,room_position_y)
	room.size = Vector2i(room_end_x,room_end_y)
	
	# Check if the new room intersects with previous rooms
	if room_not_intersects(room):
		rooms.append(room)
	
func room_not_intersects(new_room : Rect2i) -> bool:
	for room in rooms:
		if new_room.intersects(room):
			return false
	return true

